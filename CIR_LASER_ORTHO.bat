@ECHO OFF
setlocal enabledelayedexpansion
ECHO CIR ORTHO FROM RGB, NIR 
ECHO RGB,NIR in separate folder "RGB" and "NIR" 
ECHO input TIFFs RGB AND NIR have to be the same with the same names
ECHO Author: Szczepkowski Marek
ECHO Date: 22-05-2019
ECHO Version: 1.0
ECHO QGIS 2.14

SET QGIS_ROOT=C:\Program Files\QGIS 2.14
REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
SET PYTHON=C:\Users\m.szczepkowski\AppData\Local\Programs\Python\Python37\Lib\site-packages\osgeo\scripts
SET COORDINATE=2180

REM Path to working dir
SET WORK=%cd%
REM Suffix output files
SET OUT_SUFFIX=_CIR

REM COUNTER FILES
dir /b NIR\*.tif 2> nul | find "" /v /c > tmp && set /p count=<tmp && del tmp && echo %count%
set /A Counter=1

REM Path to working dir
SET WORK=%cd%
if not exist "%WORK%\CIR" mkdir "%WORK%\CIR"


FOR /F %%i IN ('dir /b "%WORK%\RGB\*.bil"') DO (
    ECHO.
	ECHO Processing  %%i    !Counter! / %count% FILES
	ECHO BUILD VRT FILES
	gdalbuildvrt "%WORK%\%%~ni_RGB_2.vrt" RGB\%%i
	gdalbuildvrt "%WORK%\%%~ni_NIR_2.vrt" NIR\%%i
	ECHO BUILD BANDS VRT FILES
	gdal_translate -of VRT -b 2 "%WORK%\%%~ni_RGB_2.vrt" "%WORK%\%%~ni_g_2.vrt"
	gdal_translate -of VRT -b 3 "%WORK%\%%~ni_RGB_2.vrt" "%WORK%\%%~ni_b_2.vrt"
	gdal_translate -of VRT -b 3 "%WORK%\%%~ni_NIR_2.vrt" "%WORK%\%%~ni_nir.vrt"
	ECHO MERGE BANDS - CREATE CIR ORTHO
	py -3.7 "%PYTHON%\gdal_merge.py" -of GTiff -separate -ot Byte -init 255 -o "%WORK%\%%~ni%OUT_SUFFIX%1.tif" "%WORK%\%%~ni_nir.vrt" "%WORK%\%%~ni_g_2.vrt" "%WORK%\%%~ni_b_2.vrt"
	gdal_translate -co "TFW=YES" "%WORK%\%%~ni%OUT_SUFFIX%1.tif" "%WORK%\CIR\%%~ni%OUT_SUFFIX%.tif"
	DEL /Q "%WORK%\*.vrt" 
	DEL /Q "%WORK%\*.tif" 
	set /A Counter+=1
)
ECHO.
ECHO Done
PAUSE