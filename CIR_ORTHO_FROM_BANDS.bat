@ECHO OFF
ECHO CIR ORTHO FROM RGB, NIR IN SEPARATE FOLDER
ECHO Author: Szczepkowski Marek
ECHO Date: 22-05-2019
ECHO Version: 1.0
ECHO QGIS 2.14
ECHO.

SET QGIS_ROOT=C:\Program Files\QGIS 2.14
REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal
SET PYTHON=C:\Users\m.szczepkowski\AppData\Local\Programs\Python\Python37\Lib\site-packages\osgeo\scripts

REM Path to working dir
SET WORK=%cd%
REM Suffix output files
SET OUT_SUFFIX=_output_CIR

ECHO BUILD VRT FILES
gdalbuildvrt RGB/RGB_2.vrt RGB/*.tif
gdalbuildvrt NIR/NIR_2.vrt NIR/*.tif
ECHO.
ECHO BUILD BANDS VRT FILES
gdal_translate -of VRT -b 2 RGB\RGB_2.vrt RGB\g_2.vrt
gdal_translate -of VRT -b 3 RGB\RGB_2.vrt RGB\b_2.vrt
gdal_translate -of VRT -b 3 NIR\NIR_2.vrt NIR\NIR.vrt
ECHO.	
ECHO MERGE BANDS - CREATE CIR ORTHO
py -3.7 "%PYTHON%\gdal_merge.py" -of GTiff -separate -ot Byte -o cir.tif NIR\NIR.vrt RGB\g_2.vrt RGB\b_2.vrt
DEL /Q "%WORK%\nir\*.vrt"
DEL /Q "%WORK%\RGB\*.vrt"
ECHO.
ECHO Done
ECHO.
PAUSE